=encoding utf8

=head1 NAME

CoreSubs::Net - Core subroutines for net 

=head1 SYNOPSIS

  use CoreSubs::Net qw(
    myname
  );

  use CoreSubs::Net qw(:all);

  myname
      $me = myname();

=head1 IMPORTANT NOTES

=over 2

=item *

=back

=head1 DESCRIPTION

=over 2

=item *

C<use CoreSubs::Net qw( XXXX ... );>

=item *

C<use CoreSubs::Net qw(:all);>

You can either specify the functions you want to import explicitly by
enumerating them between the parentheses of the "C<qw()>" operator, or
you can use the "C<:all>" tag instead to import B<ALL> available functions.

=item *

C<myname();>

This function ...

=back

=head1 VERSION

This man page documents CoreSubs version 1.0

=head1 AUTHOR

José María Alonso Josa, E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT

Copyright (c) 2018-2025 by José María Alonso Josa. All rights reserved

=head1 LICENSE

This package is free software; you can use, modify and redistribute
it under the same terms as Perl itself, i.e., at your option, under
the terms either of the "Artistic License 2.0".

=head1 BUGS

Report bugs at:

https://codeberg.org/nimiux/coresubs/issues

=cut
