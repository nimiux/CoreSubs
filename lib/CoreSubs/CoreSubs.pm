package CoreSubs::CoreSubs;

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.32';

use Exporter;
use Encode;
use vars qw($VERSION $ABSTRACT @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

$VERSION     = 1.0;
@ISA         = qw(Exporter);
@EXPORT      = ();
@EXPORT_OK   = qw(
    $DEBUG
    $NULL
    bluemsg
    boldmsg
    debugmsg
    convertbase
    donemsg_
    donemsg
    failmsg_
    failmsg
    generaterandomstring
    isxterminal
    komsg
    mydie
    myname
    notify
    okmsg
    uniq
    whoami
);

%EXPORT_TAGS = ( DEFAULT => [qw()],
                 All    => [qw()]);

use File::Basename;
use Math::Base::Convert;
use Term::ANSIColor qw(:constants);
use Text::Template;

# Environment variables
my $DEBUG = $ENV{"DEBUG"} || 0;
our $NULL = "&> /dev/null";


sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}

sub convertbase {
    my ($number, $from, $to) = @_;
    my $result;
    $result = Math::Base::Convert::cnv($number, $from, $to);
    debugmsg("Converting $number from base $from to base $to. Result: $result");
    return $result;
}

sub void {
    debugmsg("VOID");
}

sub myname {
    return basename($0);
}

sub okmsg {
    say GREEN, "@_", RESET;
}

sub komsg {
    say RED, "@_", RESET;
}

sub boldmsg {
    say BOLD, "@_", RESET;
}

sub bluemsg {
    say BOLD, BLUE, "@_", RESET;
}

sub donemsg_ {
    print BOLD, GREEN, "✓ @_", RESET;
}

sub donemsg {
    donemsg_ @_;
    print "\n";
}

sub failmsg_ {
    print BOLD, RED, "✗ @_", RESET;
}

sub failmsg {
    failmsg_ @_;
    print "\n";
}

sub debugmsg {
    say "@_" if $DEBUG ne 0;
}

sub mydie {
    komsg BOLD, RED, "@_", RESET;
    die "\n";
#   Carp::croak;
}

sub isxterminal {
    return not system "xhost >& /dev/null";
}

sub notify {
    my ($msg, $urgency) = @_;
    $urgency = $urgency || "normal";
    my $command = qq( notify-send -u $urgency "$msg" );
    debugmsg("Command: $command");
    system($command);
}

=begin

nmcli c add type wifi con-name NAME_OF_NET ifname wlo1 ssid SSID_OF_NET 
nmcli con modify NAME_OF_NET wifi-sec.key-mgmt wpa-psk
nmcli con modify NAME_OF_NET wifi-sec.psk "NAME of the net"
nmcli con up NAME_OF_NET

=end
=cut

sub whoami {
    # system "id -nu";
    return $ENV{LOGNAME} || $ENV{USER} || getpwuid($<);
}

sub motd_maker {
    my ($self) = @_;
    #if [ -x "$(which cowsay)" ]; then
    #        cows=(/usr/share/cowsay*/cows/*.cow)
    #        modes=("-b" "-d" "-g" "-s" "-t" "-w" "-y" "")
    #        filter="cowsay -f ${cows[$(($RANDOM%${#cows}))]} ${modes[$(($RANDOM%${#modes}))]}"
    #else
    #        filter="cat"
    #fi

    #if [ -x "$(which fortune)" ]; then
    #        producer="fortune"
    #else
    #        producer="uname -a"
    #fi

    #$producer | $filter > /etc/motd
}

sub adduser {
    my ($self) = @_;
    #if [ $(id -u) -eq 0 ]; then
    #    read -p "Enter username : " username
    #    read -s -p "Enter password : " password
    #    egrep "^$username" /etc/passwd > ${NULL}
    #    if [ $? -eq 0 ]; then
    #        echo "$username exists!"
    #        exit 1
    #    else
    ##        pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
    #        useradd -m -p $pass $username
    #        [ $? -eq 0 ] && echo "User has been added to system!" || echo "Failed to add a user!"
    #    fi
    #else
    #    echo "Only root may add a user to the system"
    #    exit 2
    #fi

    #read -p "Enter Username: " username
    #read -ers -p "Enter New password for user $username: " paswd
    #echo
    #read -ers -p "Enter Root Password: " rpaswd
    #echo
    #password=`python file ${paswd}`;
    #echo "$username $password $npaswd"
    #cat host | while read line
    #do
    #    #####expect####
    #    status=$(expect -c "
    #    spawn ssh $line usermod -p $password $username
    #    expect {
    #    password: { send \"$rpaswd\n\"; exp_continue }
    #    }
    #    exit
    #    ")
    #    echo ""
    #    echo "$status" > log.txt
    #    #####end of expect#######
    #done
}


sub composepasswd {
    my @alphanumeric = ('a'..'z', 'A'..'Z', 0..9);
    return join '', map $alphanumeric[rand @alphanumeric], 0..8;
}

sub generaterandomstring {
     # $chars uses Set syntax of the tr command see man tr for details.
     # For example:
     #  '+_A-Z-a-z-0-9'
     #  '12345!@#$%qwertQWERTasdfgASDFGzxcvbZXCVB'
     #  '[:alnum:]
    my $generators = [
        'tr -cd \'{$chars}\' < /dev/urandom',
        'strings /dev/urandom | tr -cd \'{$chars}\'',
        'date +%s | sha512sum | base64 | head -c {$length}',
        'dd if=/dev/urandom bs=1 count={$length} 2>/dev/null | base64 -w 0 | rev | cut -b 2- | rev',
        '< /dev/urandom tr -cd {$chars} | fold -w {$length} | head -n 1 | tr -d "\n"',
        'openssl rand -base64 {$length}',
    ];
    my $lastgeneratorindex = scalar @$generators;
    my ($length, $chars, $generator) = @_;

    $length = $length || 32;
    $chars = $chars || '+_A-Z-a-z-0-9';
    if (defined $generator) {
        $generator = $generator % $lastgeneratorindex;
    } else {
        $generator = int(rand($lastgeneratorindex));
    }
    debugmsg("Selected generator #$generator");
    my %vars = (
        'chars' => $chars,
        'length' => $length,
    );
    my $command = "$generators->[$generator] | head -c $length";
    my $template = Text::Template->new(TYPE => 'STRING', SOURCE => $command);
    my $rendered = $template->fill_in(HASH => \%vars);
    debugmsg($rendered);
    return `$rendered`;
}

sub pairs2hash {
    my %hash;
    my @pair;
    foreach ( @_ ) {
        @pair = split('=', $_);
        $hash{$pair[0]} = $pair[1];
    }
    debugmsg(Dumper(\%hash));
    return \%hash;
}

1;
