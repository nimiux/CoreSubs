package CoreSubs::Storage;

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.32';

use Exporter;
use Encode;
use vars qw($VERSION $ABSTRACT @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

$VERSION     = 1.0;
@ISA         = qw(Exporter);
@EXPORT      = ();
@EXPORT_OK   = qw(
     absolutepath
     extract
     fileext
     filename
     filepath
     filetype
     ismounted
     loopdeviceclose
     loopdeviceopen
     luks_close
     luks_open
     lvup
     maketempdir
     maketempfile
     mount
     readfromterminal
     readhashfromcsv
     readpassword
     real_path
     show_user_mounts
     sync
     umount
     vgdown
     walk
);

#    fsrandomname
#    sync
#    vgup

%EXPORT_TAGS = ( DEFAULT => [qw()],
                 All    => [qw(&evalperl &extract &loadperl &fsrandomname)]);

use Archive::Any;
use Archive::Extract;
#use Email::Sender::Simple qw(sendmail);
#use Email::Sender::Transport::SMTP::TLS ();
#use Email::Simple ();
#use Email::Simple::Creator ();
#use Email::MIME;
use Cwd qw (
    abs_path
    realpath
);
use Data::Dumper;
use Date::Calc qw (
    Today
);
use File::Basename;
use File::LibMagic;
use File::Spec::Functions qw(
    catfile
    curdir
    rel2abs
);
use File::Temp qw(
    tempfile
    tempdir
);
use IO::All;

use Term::ReadKey;
use Text::Template;
use Try::Tiny;

use CoreSubs::CoreSubs qw(
    debugmsg
    failmsg
    mydie
    myname
    notify
);

use CoreSubs::Proc qw(
    run_as_root
);

use open qw(:utf8);

my $magic = File::LibMagic->new;

my $mimes = {
    'application/vnd.oasis.opendocument.text' => 'odt',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'ppt',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'doc',
    'application/gzip' => 'gzip',
    'application/pdf' => 'pdf',
    'application/x-bzip2' => 'bzip2',
    'application/x-tar' => 'tar',
    'application/x-pie-executable' => 'elf',
    'application/x-xz' => 'xz',
    'application/zip' => 'zip',
    'audio/mpeg' => 'mp3',
    'audio/ogg' => 'ogg',
    'image/gif' => 'gif',
    'image/jpeg' => 'jpg',
    'image/png' => 'png',
    'inode/x-empty' => 'empty',
    'text/html' => 'html',
    'text/plain' => 'txt',
    'text/x-python' => 'py',
    'text/x-shellscript' => 'sh',
    'text/xml' => 'xml',
    'video/mp4' => 'mp4',
    'video/x-matroska' => 'mkv',
    'video/webm' => 'mp4',
};

sub readpassword {
    ReadMode('noecho');
    return ReadLine(0);
}

sub readfromterminal {
    my ($file, $text) = @_;
    print "Leyendo desde el terminal. ";
    if ($file) {
        say "Para finalizar pulsar Control-D)";
        while (<STDIN>) {
            $text .= $_;
        }
    } else {
        $text = <STDIN>;
    }
    return $text;
}

sub vgup {
    my ($vg) = @_;
    vgchange("y", $vg);
}

sub vgdown {
    my ($vg) = @_;
    vgchange("n", $vg);
}

sub vgchange {
    my ($upordown, $vg) = @_;
    mydie "Specify yes or no" if not $upordown;
    $vg = $vg || "";
    run_as_root("vgchange -a$upordown $vg");
}

sub lvup {
    my ($vg, $lv) = @_;
    lvchange("y", $vg, $lv);
}

sub lvdown {
    my ($vg, $lv) = @_;
    lvchange("n", $vg, $lv);
}

sub lvchange {
    my ($upordown, $vg, $lv) = @_;
    mydie "Specify yes or no" if not $upordown;
    mydie "Specify a VG" if not $vg;
    my $target = $vg;
    $lv = $lv || "";
    $target = "/dev/$vg/$lv" if $lv;
    run_as_root("lvchange -a$upordown $target");

}

sub luks_open {
    my ($device) = @_;
    my $luks_device = fsrandomname();
    run_as_root("cryptsetup luksOpen $device $luks_device");
    chomp $luks_device;
    return $luks_device;
}

sub luks_close {
    my ($luks_device) = @_;
    run_as_root("cryptsetup luksClose $luks_device");
}

sub loopdeviceopen {
    my ($filename) = @_;
    run_as_root("losetup -f $filename");
    my $loop = `losetup -j $filename | cut -d: -f 1`;
    chomp($loop);
    debugmsg("Loop device is: $loop");
    return $loop;
}

sub loopdeviceclose {
    my ($loopdevice) = @_;
    run_as_root("losetup -d $loopdevice");
}

sub mount_nfs {
    my $mounts = remotemountpoints();
    debugmsg(Dumper($mounts));
    my $ping;
    foreach ( keys %$mounts ) {
        $ping = not system "ping -c 1 $_ &> /dev/null";
        debugmsg("Result: $_ : $ping");
        for my $mountpoint ( @{$mounts->{$_}} ) {
            if ( $ping ) {
                debugmsg("mountpoint -q $mountpoint || mount $mountpoint");
                system "mountpoint -q $mountpoint || mount $mountpoint";
            } else {
                debugmsg("umount -l -f $mountpoint");
                run_as_root("umount -l -f $mountpoint");
            }
        }
    }
}

sub mount_mtp {
    # To mount all devices:
    # gio mount -li | awk -F= '{if(index(\$2,"mtp") == 1)system("gio mount "\$2)}'
    my $mtpmount = `gio mount -li | grep "mtp://" | cut -d= -f 2`;
    debugmsg("Mounting MTP: $mtpmount");
    system("gio mount $mtpmount");
}

sub show_user_mounts {
    my ($user) = @_;
    #my $media=`df -h | grep $user | grep -v $HOME | tr -s ' ' | tr ' ' '\t' | cut -f 5,6 | sed  "s_/run/media/$user/__" | sed 's/\t/-/')`;
    my $media=`df -h | grep $user | grep -v "/home/$user" | tr -s ' ' | tr ' ' '\t' | cut -f 5,6 | sed  "s_/run/media/$user/__" | sed 's/\t/-/'`;
    notify("Medios\n$media");
}

sub umount_mtp {
    my $mtpmount = `gio mount -li | grep "^Mount.*mtp ->" | cut -d' '  -f 4`;
    debugmsg("Unmounting MTP: $mtpmount");
    system("gio mount -u $mtpmount");
}

sub sync {
    my ($org, $dst, $exclude) = @_;
    if (defined $exclude) {
        my @exclude = map { "--exclude $_" } split ",", $exclude;
        $exclude = join " ", @exclude;
    } else {
        $exclude = "";
    }
    my $command = "rsync -avzt --inplace --times --delete $exclude $org $dst";
    debugmsg($command);
    return system $command;
}

sub fsrandomname {
    my $filename = basename `mktemp -u`;
    chomp $filename;
    return $filename;
}

sub ismounted {
    # Returns empty string if folder is mounted, volume mounted otherwise
    my ($folder) = @_;
    my $volume = "";
    my $notmounted = system "mountpoint -q $folder";
    $volume = `df -P $folder | tail -1 | cut -d' ' -f 1` unless $notmounted;
    return $volume;
}

sub makefs {
    my ($type, $volume) = @_;

    run_as_root("mkfs -t $type $volume");
}

sub mount {
    my ($volume, $mountpoint) = @_;
    $mountpoint = $mountpoint || "";
    debugmsg("mount $volume $mountpoint");
    return run_as_root("mount $volume $mountpoint");
}

sub mountpartition {
    my ($self) = @_;
    #local diskimg="${1}"
    #local partnumber="${2}"
    #local mountpoint="${3}"
    #local sb=$(getstartbyte ${diskimg} ${partnumber})
    #mount -o loop,offset=${sb} ${diskimg} ${mountpoint}
}

sub mountloop {
    my ($file, $folder) = @_;
    return run_as_root("mount -o loop $file $folder");
}

sub mounttmp {
    my ($folder, $fsize) = @_;

    return run_as_root("");
}

sub umount {
    foreach ( @_ ) {
        debugmsg("umounting $_");
        run_as_root("umount $_");
   }
}

sub remotemountpoints {
    my @mounts = `sed -e '/^.*#/d' -e '/^.*:/!d' -e 's/\t/ /g' /etc/fstab | tr -s " "`;
    my $server;
    my %mounts;
    foreach ( @mounts ) {
        $server = (split ":")[0];
        push @{$mounts{$server}}, (split " ")[1];
    }
    return \%mounts;
}

#sub create_path {
    #my ($self) = @_;
    ##local path=${1}
#
    ##mkdir -p ${path}
#}

sub splitfilepath {
    my ($filepath) = @_;
    debugmsg("Full Path is $filepath");
    #my ($base, $dir, $ext) = fileparse($filepath, qr/\.[^.]*/);
    #debugmsg("Base is $base. Dir is $dir. Ext is $ext");
    #return ($base, $dir, $ext);
    return fileparse($filepath, qr/\.[^.]*/);
}

sub absolutepath {
    my ($path) = @_;
    my $abspath = rel2abs $path;
    debugmsg("Absolute path of $path is $abspath");
    return $abspath;
}

sub real_path {
    my ($path) = @_;
    my $realpath = realpath $path;
    debugmsg( "Real path of $path is $realpath" );
    return $realpath;
}

sub filepath {
    my ($filepath) = @_;
    debugmsg("Path is $filepath");
    my ($base, $dir, $ext) = splitfilepath($filepath);
    debugmsg("Returning directory: $dir");
    return $dir;
}

sub filename {
    my ($filepath) = @_;
    debugmsg("Path is $filepath");
    my ($base, $dir, $ext) = splitfilepath($filepath);
    debugmsg("Base is $base");
    return $base;
}

sub fileext {
    # bash: echo "${path##*.}"
    my ($filepath) = @_;
    debugmsg("Path is $filepath");
    my ($base, $dir, $ext) = splitfilepath($filepath);
    debugmsg("Extension is $ext");
    # drop heading .
    return substr $ext, 1;
}

sub filetype {
    my ($filename) = @_;
    my $info = $magic->info_from_filename($filename);
    #debugmsg(Dumper($info));
    my $mimetype = $info->{mime_type};
    return exists($mimes->{$mimetype}) ? $mimes->{$mimetype} : "MIME $mimetype not found in my mimes";
}

sub maketempdir {
    return tempdir();
}

sub can_execute {
    my ($self) = @_;
    #local file="${1}"
    #[[ -x ${file} ]]
}

sub listpath {
    my ($path, $sort) = @_;
    opendir my $dh, $path or warn "Error listing $path: $!";
    my @items = grep !/^\.\.?$/, readdir $dh;
    closedir $dh;
    @items = map { decode( 'utf8', $_ ) } sort {$a cmp $b} @items;
    return \@items;
}

sub walk {
    my ($path, $function, $recurse, @extra) = @_;
    $recurse = $recurse || 0;
    my $items = listpath($path);
    debugmsg(Dumper($items));
    my @items = sort { "\L$a" cmp "\L$b" } @{$items};
    debugmsg(Dumper(@items));
    foreach my $item (@items) {
        my $itemfullpath = catfile($path, $item);
        debugmsg("Processing item $itemfullpath");
        $function->($itemfullpath, @extra);
        if (-d $itemfullpath && $recurse != 0) {
            my @args = ($itemfullpath, $function, $recurse, @extra);
            walk(@args);
        }
    }
}

sub changeowner {
    my ($self) = @_;
    #local owner=${1}
    #shift
    #local objects=${*}

    #change_owner ${owner} ${objects}
}

sub clean_old_files {
    my ($self) = @_;
    #local path="${1}"
    #local days="${2}"
    #local user="${3}"

    #for file in $(find ${path} -mtime +${days}) ; do
        #echo "Removing file ${file}"
        #run_as_user ${user} rm ${file}
    #done
}

sub make_chroot {
    my ($self) = @_;
    #local dir=${1}
    #for a in ${@:2:$#} ; do
    #set -- ${a} ${dir}
    #local var=$(ldd $(which ${1}) | awk '{print $1"\n"$3 ; }' | grep '/' );
    #{
    #cp -a --parents ${var} ${2}
    ##cp --parents ${var} ${2}
    #cp --parents $(which ${1}) ${2}
    #} 2> /dev/null;
    #done
}

sub dochroot {
    my ($self) = @_;
    #local rootdir="${1}"
    #local rcfile="${2}"
    #local arch="32"

    #[ -d "${rootdir}/lib64" ] && arch="64"

    #echo "EXECUTING: linux${arch} chroot ${rootdir} /bin/bash --rcfile ${rcfile}"
    #linux${arch} chroot ${rootdir} /bin/bash --rcfile ${rcfile}
}

sub fileusage {
    my ($path) = @_;
    run_as_root("du -a $path | sort -n -r | head -n 10")
}

sub maketempfile {
    my ($fh, $filename) = tempfile();
    return $filename;
}

sub makefile {
    my ($name, $size) = @_;

    # Other ways to create a zeroed file:
    # dd if=/dev/zero of=${file} bs=${size} count=1 2> ${NULL}
    # fallocate -l ${size} ${file}
    return system "truncate -s $size $name";
}

sub filesize {
    my ($filename) = @_;

    return `stat -c "%s" $filename`
}

sub difffiles {
    my ($firstfile, $secondfile) = @_;

    my $sum1 = sha($firstfile);
    my $sum2 = sha($secondfile);
    return ($sum1 == $sum2);
}

sub filelines {
    my ($filename, $start, $end) = @_;
    $start = $start || '1';
    $end = $end || '\$';
    `sed -n -e "${start},${end}p" ${filename}`
}

sub timestamp {
    my $filename = `date +"%Y%m%d%H%M%S"`;
    return system "date +\"%a, %d %b %Y %H:%M:%S\" > $filename";
}

sub extract {
    my ($archive, $dstpath) = @_;
    my ($ae, $files);
    $dstpath = $dstpath || tempdir();
    debugmsg("Extracting file $archive to $dstpath");
    if (filetype($archive) eq 'zip') {
        # Archive:Extract does not work of for zips...
        $ae = Archive::Any->new($archive);
        $ae->extract($dstpath) or mydie "Error Extrayendo $archive";
        my @files = $ae->files;
        $files = \@files;
    } else {
        $ae = Archive::Extract->new( archive => $archive )
            or mydie "Unable to extract file $archive";
        my $ok = $ae->extract(to => $dstpath) or mydie $ae->error;
        $archive = $ae->{archive};
        $dstpath = $ae->{extract_path};
        $files = $ae->{files};
    }
    debugmsg("Archive $archive extracted to $dstpath");
    debugmsg("Files is: $files");
    return $files;
}

sub readhashfromcsv {
    my ($filename, $headerlabel, $separator, $key, $keyvalue, $hashkey, $commentchar) = @_;
    $headerlabel or die "Invalid header label: $headerlabel";
    $separator = $separator || ':';
    $commentchar = $commentchar || '#';
    my %hash;
    debugmsg("Reading CSV file: $filename. Separator: $separator");
    debugmsg("Key: $key. Key value: $keyvalue. Hash key: $hashkey") if $key;
    open my $fh, "-|", "grep -v '$commentchar' $filename" or die "Could not open file $filename: $!";
    my $header = '';
    while (<$fh>) {
        # TODO Beware with separator, we supose headers is using : for separating fields
        if (/^$headerlabel:/) {
            $header = $_;
            last;
        }
    }
    my $csv = Text::CSV->new({ sep_char => $separator }) or die "Error Text::CSV: " . Text::CSV->error_diag;
    $csv->parse($header);
    $csv->column_names([$csv->fields]);
    my $counter = 1;
    while (my $row = $csv->getline_hr($fh)) {
        if (defined $key) {
            next if $row->{$key} ne $keyvalue;
        }
        my $hashkeyvalue = $row->{$hashkey};
        debugmsg(Dumper { $hashkeyvalue => $row });
        $hash{$hashkeyvalue} = $row;
    }
    $csv->eof or $csv->error_diag;
    close $fh;
    return \%hash;
}

1;
