[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

CoreSubs version 1.0
====================

The README is used to introduce the module and provide instructions on
how to install the module, any machine dependencies it may have (for
example C compilers and installed libraries) and any other information
that should be provided before the module is installed.

A README file is required for CPAN modules since CPAN extracts the
README file from a module distribution so that people browsing the
archive can use it get an idea of the modules uses. It is usually a
good idea to provide version information here so that people can
decide whether fixes for the module are worth downloading.

INSTALLATION

Install and configure cpan for your distro (normally ships with perl)
Install libmagic library for your distro (For Debian distros: libmagic-dev)
Run: cpan File::LibMagic File::Slurp
Run this project install.sh  script

DEPENDENCIES

This module requires these other modules and libraries:

Getopt::Long::Descriptive
Archive::Any
Archive::Extract
Email::Sender::Simple
Email::Sender::Transport::SMTP::TLS
Email::MIME
Date::Calc
File::LibMagic
IO::All
Term::ReadKey
Text::Template

To recreate CPAN modules:

cpan All::MigrateModules 
mv perl5 perl5_old
perl-migrate-modules --from perl5_old /usr/bin/perl

USAGE


SEE ALSO

URI::Escape;
LWP::Simple
Mail::Box
Net::SMTP
Net::POP3
Net::DNS
Net::Ping
XML::RSS
XML::Simple
XML::SAX
Getopt::*


NOTES

Use the following to generate a new module template:

h2xs -n MyModule -AX

ON PERL MODULES (by tachyon)
Fixed a typo and added a few comments. Thanks to John M. Dlugosz. Rewrote and restyled tute for compatibility with versions of Perl < 5.6 thanks to crazyinsomniac. Also thanks to tye for reminding me that $^W++ is globally scoped and a bit antisocial for a module.

MyModule.pm

package MyModule;

use strict;
use Exporter;
# We pacify strict with the use vars declaration of some variables. We can use an 'our' declaration in 5.6+
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

# We now set a $VERSION number and make Exporter part of MyModule using the @ISA. See perlboot for all the gory details on what @ISA is or just use it as shown.
$VERSION     = 1.00;
@ISA         = qw(Exporter);
# @EXPORT contains a list of functions that we export by default, in this
# case nothing. Generally the less you export by default using @EXPORT
# the better. This avoids accidentally clashing with functions defined
# in the script using the module. If a script wants a function let it ask.
@EXPORT      = ();
# @EXPORT_OK contains a list of functions that we export on demand so we
# export &func1 &func2 only if specifically requested to. Use this in
# preference to just blindly exporting functions via @EXPORT.
# You can also export variables like $CONFIG provided they are globals
# not lexicals scoped with my (read declare them with our or use vars).
@EXPORT_OK   = qw(func1 func2);
# %EXPORT_TAGS. For convenience we define two sets of export tags.
# The ':DEFAULT' tag exports only &func1; the ':Both' tag exports
# both &func1 &func2. This hash stores labels pointing to array
# references. In this case the arrays are anonymous.
%EXPORT_TAGS = ( DEFAULT => [qw(&func1)],
                 Both    => [qw(&func1 &func2)]);

sub func1  { return reverse @_  }
sub func2  { return map{ uc }@_ }

# We need the 1; at the end because when a module loads Perl checks to
# see that the module returns a true value to ensure it loaded OK.
# You could put any true value at the end (see Code::Police) but 1 is
# the convention.
1;

MyScript.pl (A simple script to use MyModule)

#!/usr/bin/perl -w

use strict;

# you may need to set @INC here (see below)

my @list = qw (J u s t ~ A n o t h e r ~ P e r l ~ H a c k e r !);

# case 1
# use MyModule;
# print func1(@list),"\n";
# print func2(@list),"\n";
# Case 1: Because our module exports nothing by default we get errors
# as &funct1 and &funct2 have not been exported thus do not exist in
# the main:: namespace of the script.

# case 2
# use MyModule qw(&func1);
# print func1(@list),"\n";
# print MyModule::func2(@list),"\n";
# Case 2: This works OK. We ask our module to export the &func1 so we
# can use it. Although &func2 was not exported we reference it with
# its full package name so this works OK.

# case 3
# use MyModule qw(:DEFAULT);
# print func1(@list),"\n";
# print func2(@list),"\n";
# Case 3: The ':DEFAULT' tag *should* export &func1 so you might expect
# the error here to concern a missing &func2. In fact Perl complains
# about &func1. Hmm, what is going on here. The DEFAULT tag name is
# special and is automatically set in our modules %EXPORT_TAGS hash
# like this DEFAULT => \@EXPORT.

# case 4
# use MyModule qw(:Both);
# print func1(@list),"\n";
# print func2(@list),"\n";
# Case 4: We specified the export of both our functions with the
# ':Both' thus this works.

A note on @INC

When you issue a use MyModule; directive perl searchs the @INC array for a module with the correct name. @INC usually contains:

/perl/lib
/perl/site/lib
.

The . directory (dot dir) is the current working directory. CORE modules are installed under perl/lib whereas non-CORE modules install under perl/site/lib. You can add directories to the module search path in @INC like this:

BEGIN { push @INC, '/my/dir' }
# or
BEGIN { unshift @INC, '/my/dir' }
# or
use lib '/my/dir';
[download]

We need to use a BEGIN block to shift values into @INC at compile time as this is when perl checks for modules. If you wait until the script is comiled it is too late and perl will throw an exception saying it can't find MyModule in @INC... The difference between pushing a value and unshifting a value into @INC is that perl searches the @INC array for the module starting with the first dir in that array. Thus is you have a MyModule in /perl/lib/ and another in /perl/site/lib/ and another in ./ the one in /perl/lib will be found first and thus the one used. The use lib pragma effectively does the same as the BEGIN { unshift @INC, $dir } block - see perlman:lib:lib for full specifics.
What use Foo::Bar means

use Foo::Bar does not mean look for a module called "Foo::Bar.pm" in the @INC directories. It means search @INC for a *subdir* called "Foo" and a *module* called "Bar.pm".

Now once we have "use'd" a module its functions are available via the fully specified &PACKAGE::FUNCTION syntax. When we say &Foo::Bar::some_func we are refering to the *package name* not the (dir::)file name that we used in the use. This allows you to have many package names in one use'd file. In practice the names are usually the same.
use Warnings;

You should test your module with warnings enabled as this will pick up many subtle (and not so subtle :-) errors. You can activate warnings using the -w flag in the script you use to test the module. If you add use warnings to the module then your module will require Perl 5.6+ as this was not available before then. If you put $^W++ at the top of the module then you will globally enable warnings - this may break *other modules* a script may be using in addition to your module so is rather antisocial. An expert coder here called tye tests with warnings but does not include them directly in his/her modules.

COPYRIGHT AND LICENCE

Copyright (C) 2019-2020 by José María Alonso Josa <nimiux@freeshell.de>

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.30.0 or,
at your option, any later version of Perl 5 you may have available.
